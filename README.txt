Minetest Game mod: timer
==========================
See license.txt for license information.

This mod provides a simple countdown timer for each player. It uses xpfw for storing value, so it is also visible in the xpfw hud.

chat command:

/cd <value> 	set countdown with <value> seconds. if not given, start with default value
/cd r/rest		give remainign countdown time

Authors of source code
----------------------
ademant (MIT)

Authors of media (textures)
---------------------------
  
Created by ademant (CC BY 3.0):
